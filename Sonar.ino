
// Сонар
#define ECHO 2
#define TRIG 3
#define sensVCC 4


#include <NewPing.h>
NewPing sonar(TRIG, ECHO, 400);

float dist_3[3] = {0.0, 0.0, 0.0};   // Три останні вимірювання
float dist, dist_filtered;
float k;
byte i, delta;
unsigned long sensTimer;

void setup() {
  Serial.begin(9600);
  // Налаштування піна
  pinMode(sensVCC, OUTPUT);
  // Подаєм сигнал на пін
  digitalWrite(sensVCC, 1);

}

void loop() 
{
  if (millis() - sensTimer > 20) {                          // Вимір кожні 20 мілісекунд або 50 раз на секунду
    if (i > 1) i = 0;                                       // По кругу вибираємо елемент масиву
    else i++;

    dist_3[i] = (float)sonar.ping() / 57.5;                 // отримати відстань

    dist = middle_of_3(dist_3[0], dist_3[1], dist_3[2]);    // Відфільтрувати медіанним фільтром



    delta = abs(dist_filtered - dist);
    if (delta > 1) k = 0.7;
    else k = 0.1;

    dist_filtered = dist * k + dist_filtered * (1 - k);     // Running average фільтр

    Serial.println(dist);

    sensTimer = millis();                                   // Скинути таймер
  }
}

// Медіанний фільтр із трьох значень
float middle_of_3(float a, float b, float c) {
  float middle;
  if ((a <= b) && (a <= c)) {
    middle = (b <= c) ? b : c;
  }
  else {
    if ((b <= a) && (b <= c)) {
      middle = (a <= c) ? a : c;
    }
    else {
      middle = (a <= b) ? a : b;
    }
  }
  return middle;
}
